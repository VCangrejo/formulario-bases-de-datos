package formulario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class ConectorDB {

    Connection miconexion; //clase para conectarnops//
    PreparedStatement clausula; //clase para crear clausulas sql//

    boolean conectar() {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            miconexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/base1?autoReconnect=true&useSSL", "root", "mysql2019");

        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    boolean escribir() {

        int PH = 2;
        float temperatura = (float) 12.5;
        String color = "azul";
        String humedad = "media";

        try {
            String insertar = "INSERT INTO `base1`.`opciones` (`PH`, `temperatura`, `color`, `humedad`) VALUES (?,?,?,?)";
            clausula = miconexion.prepareStatement(insertar);
            clausula.setInt(1,PH);
            clausula.setFloat(2, temperatura);
            clausula.setString(3, color);
            clausula.setString(4, humedad);
            
                 
        } catch (Exception e) {
            System.out.println(e);
        }

        return true;
    }
}
